package com.gmail.jd4656.ontime;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandGraylist implements TabExecutor {
    private OnTime plugin;

    CommandGraylist(OnTime p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ontime.graylist")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GREEN + "Graylist commands:");
            sender.sendMessage(ChatColor.GOLD + "/graylist <time> - Turns on graylist for players with less than the specified amount of ontime.");
            sender.sendMessage(ChatColor.GOLD + "/graylist off - Turns off the graylist.");
            sender.sendMessage(ChatColor.GOLD + "/graylist status - Displays the status of graylist.");
            sender.sendMessage(ChatColor.GOLD + "/graylist reload - Reloads the configuration.");
            return true;
        }

        if (args[0].equals("off")) {
            if (plugin.enabled < 0) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The graylist is not currently enabled.");
                return true;
            }

            plugin.enabled = -1;
            Bukkit.broadcast(ChatColor.DARK_RED + "[GrayList] " + ChatColor.RED + "Graylist has been disabled by " + sender.getName(), "ontime.graylist.notify");
            sender.sendMessage(ChatColor.DARK_GREEN + "The graylist has been disabled.");
            return true;
        }

        if (args[0].equals("reload")) {
            plugin.reloadConfig();
            plugin.config = plugin.getConfig();
            sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
            return true;
        }

        if (args[0].equals("status")) {
            if (plugin.enabled == -1) {
                sender.sendMessage(ChatColor.RED + "Graylist is not enabled.");
                return true;
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "Graylist is currently set to " + convertTime(plugin.enabled));
            return true;
        }

        long time = stringToTime(args[0]);

        if (time == 0) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid time format.");
            return true;
        }

        plugin.enabled = time;

        List<Player> kickedPlayers = new ArrayList<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (plugin.getOnTime(player.getUniqueId()) < time) kickedPlayers.add(player);
        }

        String kickMsg = plugin.getKickMessage();

        for (Player player : kickedPlayers) {
            player.kickPlayer(kickMsg);
        }

        Bukkit.broadcast(ChatColor.DARK_RED + "[GrayList] " + ChatColor.GREEN + "Graylist has been enabled by " + sender.getName(), "ontime.graylist.notify");
        sender.sendMessage(ChatColor.DARK_GREEN + "The graylist has been enabled. New users with less than " + convertTime(time) + " of playtime will not be able to join.");
        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (sender.hasPermission("ontime.graylist")) {
            if (args.length == 1) {
                tabComplete.add("<time>");
                tabComplete.add("off");
                tabComplete.add("reload");
                tabComplete.add("status");
            }
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }

    private String convertTime(long ms) {
        long seconds = (ms / 1000) % 60L;
        long minutes = (ms / (1000 * 60)) % 60L;
        long hours = (ms / (1000 * 60 * 60)) % 24L;
        long days = (ms / (1000 * 60 * 60 * 24L));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    private long stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        long timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }


        if (timeModifier == 's') {
            timeDuration = timeNumber * 1000L;
        }
        if (timeModifier == 'm') {
            timeDuration = timeNumber * 60 * 1000L;
        }
        if (timeModifier == 'h') {
            timeDuration = timeNumber * 60 * 60 * 1000L;
        }
        if (timeModifier == 'd') {
            timeDuration = timeNumber * 24 * 60 * 60 * 1000L;
        }

        return timeDuration;
    }
}
