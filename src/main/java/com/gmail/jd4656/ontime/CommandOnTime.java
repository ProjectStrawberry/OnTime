package com.gmail.jd4656.ontime;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.StringUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CommandOnTime implements TabExecutor {

    OnTime plugin;

    CommandOnTime(OnTime p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("ontime.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length < 1) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            player.performCommand("ontime lookup " + player.getName());
            return true;
        }

        if (args[0].equals("top")) {
            long time = 0;
            String leaderboardType = "Total";
            int page = 1;
            if (args.length > 1) {
                if (args[1].equals("day")) {
                    leaderboardType = "Daily";
                    time = 24 * 60 * 60 * 1000;
                }
                if (args[1].equals("week")) {
                    leaderboardType = "Weekly";
                    time = 7 * 24 * 60 * 60 * 1000;
                }
                if (args[1].equals("month")) {
                    leaderboardType = "Monthly";
                    time = 30 * 24 * 60 * 60 * 1000;
                }

                if (time != 0) time = System.currentTimeMillis() - time;

                if (args.length > 2) {
                    try {
                        page = Integer.parseInt(args[2]);
                    } catch (NumberFormatException ignored) {}
                }
            }

            page *= 10;

            final long finalTime = time;
            final int finalPage = page;
            final String type = leaderboardType;

            new BukkitRunnable() {
                @Override
                public void run() {
                    Map<UUID, Long> ontime = new LinkedHashMap<>();
                    try {
                        Connection con = plugin.getConnection();
                        PreparedStatement pstmt = con.prepareStatement("SELECT ANY_VALUE(uuid),SUM(duration) FROM " + plugin.mysqlConfig.getTableName("ontime") + " WHERE loginTime >= ? GROUP BY uuid ORDER BY SUM(duration) DESC LIMIT ?, ?");
                        pstmt.setLong(1, finalTime);
                        pstmt.setInt(2, finalPage - 10);
                        pstmt.setInt(3, 10);

                        ResultSet rs = pstmt.executeQuery();

                        while (rs.next()) {
                            ontime.put(UUID.fromString(rs.getString("ANY_VALUE(uuid)")), rs.getLong("SUM(duration)"));
                        }

                        rs.close();
                        pstmt.close();

                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (ontime.size() < 1) {
                                    sender.sendMessage(plugin.parseColorCodes("#d6ab33") + "There is not enough ontime data for the last " + type.toLowerCase() + ".");
                                    return;
                                }

                                BaseComponent[] logo = null;
                                BaseComponent[] header = null;
                                BaseComponent[] footer = null;

                                int currentPage = (finalPage / 10);

                                if (type.equals("Total")) {
                                    header = TextComponent.fromLegacyText(plugin.parseColorCodes("#6a5b87>>#cf537a--#e66376--#fc8b8b---#e66376--#cf537a--#6a5b87<{#e6d2d1Total Leaderboard#6a5b87}>#cf537a--#e66376--#fc8b8b---#e66376--#cf537a--#6a5b87<<"));
                                    logo = TextComponent.fromLegacyText(" ".repeat(27) + plugin.parseColorCodes("#6a5b87>>{#e6d2d1Ontime#6a5b87}<<"));

                                    ComponentBuilder builder = new ComponentBuilder();
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#6a5b87>>#cf537a--#fc8b8b---#cf537a--")));

                                    TextComponent prev = new TextComponent();
                                    prev.setText("[prev]");
                                    prev.setColor(ChatColor.of("#909ead"));
                                    prev.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top total " + (currentPage == 1 ? 1 : currentPage - 1)));

                                    builder.append(prev);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#cf537a-----#6a5b87{#e6d2d1Page " + currentPage +"#6a5b87}#cf537a-----")));

                                    TextComponent next = new TextComponent();
                                    next.setText("[next]");
                                    next.setColor(ChatColor.of("#fc8f53"));
                                    next.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top total " + (currentPage + 1)));

                                    builder.append(next);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#cf537a--#fc8b8b---#cf537a--#6a5b87<<")));

                                    footer = builder.create();
                                }

                                if (type.equals("Daily")) {
                                    header = TextComponent.fromLegacyText(plugin.parseColorCodes("#f59090>>#c72828--#e03434--#f55656---#e03434--#c72828--#f59090<{#f5d6ceDaily Leaderboard#f59090}>#c72828--#e03434--#f55656---#e03434--#c72828--#f59090<<"));
                                    logo = TextComponent.fromLegacyText(" ".repeat(27) + plugin.parseColorCodes("#6a5b87>>{#e6d2d1Ontime#6a5b87}<<"));

                                    ComponentBuilder builder = new ComponentBuilder();
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#f59090>>#c72828--#f55656---#c72828--")));

                                    TextComponent prev = new TextComponent();
                                    prev.setText("[prev]");
                                    prev.setColor(ChatColor.of("#909ead"));
                                    prev.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top day " + (currentPage == 1 ? 1 : currentPage - 1)));

                                    builder.append(prev);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#c72828-----#f59090{#f5d6cePage " + currentPage + "#f59090}#c72828-----")));

                                    TextComponent next = new TextComponent();
                                    next.setText("[next]");
                                    next.setColor(ChatColor.of("#798bdb"));
                                    next.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top day " + (currentPage + 1)));

                                    builder.append(next);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#c72828--#f55656---#c72828--#f59090<<")));

                                    footer = builder.create();
                                }

                                if (type.equals("Weekly")) {
                                    header = TextComponent.fromLegacyText(plugin.parseColorCodes("#258052>>#548f32--#68a147--#8dc26e---#68a147--#548f32--#258052<{#cce895Weekly Leaderboard#258052}>#548f32--#68a147--#8dc26e---#68a147--#548f32--#258052<<"));
                                    logo = TextComponent.fromLegacyText(" ".repeat(27) + plugin.parseColorCodes("#6a5b87>>{#e6d2d1Ontime#6a5b87}<<"));

                                    ComponentBuilder builder = new ComponentBuilder();
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#258052>>#548f32--#8dc26e---#548f32--")));

                                    TextComponent prev = new TextComponent();
                                    prev.setText("[prev]");
                                    prev.setColor(ChatColor.of("#909ead"));
                                    prev.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top week " + (currentPage == 1 ? 1 : currentPage - 1)));

                                    builder.append(prev);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#548f32-----#258052{#cce895Page " + currentPage + "#258052}#548f32-----")));

                                    TextComponent next = new TextComponent();
                                    next.setText("[next]");
                                    next.setColor(ChatColor.of("#e374b8"));
                                    next.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top week " + (currentPage + 1)));

                                    builder.append(next);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#548f32--#8dc26e---#548f32--#258052<<")));

                                    footer = builder.create();
                                }

                                if (type.equals("Monthly")) {
                                    header = TextComponent.fromLegacyText(plugin.parseColorCodes("#864aff>>#5640e6--#4067e6--#4098e6---#4067e6--#5640e6--#864aff<{#94c6f2Monthly Leaderboard#864aff}>#5640e6--#4067e6--#4098e6---#5640e6--#4067e6--#864aff<<"));
                                    logo = TextComponent.fromLegacyText(" ".repeat(27) + plugin.parseColorCodes("#6a5b87>>{#e6d2d1Ontime#6a5b87}<<"));

                                    ComponentBuilder builder = new ComponentBuilder();
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#864aff>>#5640e6--#4098e6---#5640e6--")));

                                    TextComponent prev = new TextComponent();
                                    prev.setText("[prev]");
                                    prev.setColor(ChatColor.of("#909ead"));
                                    prev.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top month " + (currentPage == 1 ? 1 : currentPage - 1)));

                                    builder.append(prev);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#5640e6-----#864aff{#94c6f2Page " + currentPage + "#864aff}#5640e6-----")));

                                    TextComponent next = new TextComponent();
                                    next.setText("[next]");
                                    next.setColor(ChatColor.of("#4ac74e"));
                                    next.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ontime top month " + (currentPage + 1)));

                                    builder.append(next);
                                    builder.append(TextComponent.fromLegacyText(plugin.parseColorCodes("#5640e6--#4098e6---#5640e6--#864aff<<")));

                                    footer = builder.create();
                                }

                                sender.spigot().sendMessage(logo);
                                sender.spigot().sendMessage(header);

                                int count = (finalPage - 10) + 1;
                                for (Map.Entry<UUID, Long> entry : ontime.entrySet()) {
                                    UUID id = entry.getKey();
                                    OfflinePlayer curPlayer = Bukkit.getOfflinePlayer(id);
                                    if (curPlayer.getName() == null) continue;

                                    if (type.equals("Total")) {
                                        sender.sendMessage(plugin.parseColorCodes("#6a5b87" + count + ". #e6d2d1" +
                                                formatLine(curPlayer.getName()) +
                                                plugin.convertTimeColours(entry.getValue(), "#cf537a", "#e66376", "#fc8b8b")));
                                    }

                                    if (type.equals("Daily")) {
                                        sender.sendMessage(plugin.parseColorCodes("#f59090" + count + ". #f5d6ce" +
                                                formatLine(curPlayer.getName()) +
                                                plugin.convertTimeColours(entry.getValue(), "#f55656", "#e03434", "#c72828")));
                                    }

                                    if (type.equals("Weekly")) {
                                        sender.sendMessage(plugin.parseColorCodes("#258052" + count + ". #cce895" +
                                                formatLine(curPlayer.getName()) +
                                                plugin.convertTimeColours(entry.getValue(), "#548f32", "#68a147", "#8dc26e")));
                                    }

                                    if (type.equals("Monthly")) {
                                        sender.sendMessage(plugin.parseColorCodes("#864aff" + count + ". #94c6f2" +
                                                formatLine(curPlayer.getName()) +
                                                plugin.convertTimeColours(entry.getValue(), "#5640e6", "#4067e6", "#4098e6")));
                                    }

                                    count++;
                                }

                                sender.spigot().sendMessage(footer);
                            }
                        }.runTask(plugin);
                    } catch (SQLException | ClassNotFoundException e) {
                        plugin.getLogger().warning("Error getting ontime leaderboards: ");
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(plugin);

            return true;
        }

        if (args[0].equals("lookup")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GREEN + "Usage: " + ChatColor.DARK_GREEN + "/ontime lookup <player>");
                return true;
            }

            String targetName = args[1];
            Player targetPlayer = Bukkit.getPlayer(targetName);

            new BukkitRunnable() {
                @Override
                public void run() {
                    UUID id = (targetPlayer == null ? plugin.getUUID(targetName) : targetPlayer.getUniqueId());

                    if (id == null) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                            }
                        }.runTask(plugin);
                        return;
                    }

                    long overall = plugin.getOnTime(id);
                    long month = plugin.getOnTime(id, System.currentTimeMillis() - (31L * 24 * 60 * 60 * 1000));
                    long week = plugin.getOnTime(id, System.currentTimeMillis() - (7 * 24 * 60 * 60 * 1000));
                    long day = plugin.getOnTime(id, System.currentTimeMillis() - (24 * 60 * 60 * 1000));
                    long joinDate = plugin.getJoinTime(id);
                    long lastOnline = plugin.getLastOnline(id);

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Date date = new Date();
                            date.setTime(joinDate);

                            OfflinePlayer targetPlayer = Bukkit.getOfflinePlayer(id);

                            sender.sendMessage(" ".repeat(25) + plugin.parseColorCodes("#6a5b87>>{#e6d2d1OnTime#6a5b87}<<"));
                            sender.sendMessage(plugin.parseColorCodes("#6a5b87>>#cf537a--#e66376--#fc8b8b---#e66376--#cf537a--#6a5b87<{#e6d2d1" + targetPlayer.getName() + "'s OnTime#6a5b87}>#cf537a--#e66376--#fc8b8b---#e66376--#cf537a--#6a5b87<<"));

                            sender.sendMessage(plugin.parseColorCodes("#d6ab33Join Date: ") + new SimpleDateFormat("MMMM d, yyyy").format(new Date(joinDate)));
                            if (!plugin.loginTime.containsKey(id)) {
                                sender.sendMessage(plugin.parseColorCodes("#d6ab33Last Online: ") + plugin.convertTimeColours((System.currentTimeMillis() - lastOnline), "#cf537a", "#e66376", "#fc8b8b"));
                            }
                            if (overall > 0) sender.sendMessage(plugin.parseColorCodes("#f2cb5eOverall: ") + plugin.convertTimeColours(overall, "#cf537a", "#e66376", "#fc8b8b"));
                            if (month > 0) sender.sendMessage(plugin.parseColorCodes("#f2cb5eMonth: ") + plugin.convertTimeColours(month, "#cf537a", "#e66376", "#fc8b8b"));
                            if (week > 0) sender.sendMessage(plugin.parseColorCodes("#f2cb5eWeek: ") + plugin.convertTimeColours(week, "#cf537a", "#e66376", "#fc8b8b"));
                            if (day > 0) sender.sendMessage(plugin.parseColorCodes("#f2cb5eDay: ") + plugin.convertTimeColours(day, "#cf537a", "#e66376", "#fc8b8b"));
                            if (plugin.loginTime.containsKey(id)) sender.sendMessage(plugin.parseColorCodes("#f2cb5eSession: ") + plugin.convertTimeColours(System.currentTimeMillis() - plugin.loginTime.get(id), "#cf537a", "#e66376", "#fc8b8b"));
                        }
                    }.runTask(plugin);
                }
            }.runTaskAsynchronously(plugin);
        }

        return true;
    }

    public String formatLine(String str) {
        if (str.length() < 20) str += " ".repeat(20 - str.length());
        return str;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (sender.hasPermission("ontime.use")) {
            if (args.length == 1) {
                tabComplete.add("lookup");
                tabComplete.add("top");
            }
            if (args.length == 2) {
                if (args[0].equals("lookup")) tabComplete.addAll(plugin.nameCache);
                if (args[0].equals("top")) tabComplete.addAll(Arrays.asList("day", "week", "month", "total"));
            }
            if (args.length == 3) {
                if (args[0].equals("top")) tabComplete.add("<page>");
            }
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
