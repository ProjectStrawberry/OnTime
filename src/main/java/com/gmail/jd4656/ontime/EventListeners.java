package com.gmail.jd4656.ontime;

import me.leoko.advancedban.manager.PunishmentManager;
import me.leoko.advancedban.manager.UUIDManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EventListeners implements Listener {
    private OnTime plugin;
    private Map<UUID, IPContainer> users = new HashMap<>();

    EventListeners(OnTime p) {
        plugin = p;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        plugin.loginTime.put(event.getPlayer().getUniqueId(), System.currentTimeMillis());
        plugin.paidTime.put(event.getPlayer(), 0);
        plugin.nameCache.add(event.getPlayer().getName());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.paidTime.remove(player);
        if (!plugin.loginTime.containsKey(player.getUniqueId())) return;
        long joinTime = plugin.loginTime.get(player.getUniqueId());
        plugin.loginTime.remove(player.getUniqueId());

        new BukkitRunnable() {
            @Override
            public void run() {
                plugin.updateTime(player, joinTime, (System.currentTimeMillis() - joinTime));
            }
        }.runTaskAsynchronously(plugin);
    }

    @EventHandler (priority=EventPriority.LOWEST)
    public void onLogin(AsyncPlayerPreLoginEvent event) {
        if (plugin.enabled > -1) {
            UUID uuid = event.getUniqueId();
            IPContainer ipContainer = (users.containsKey(uuid) ? users.get(uuid) : new IPContainer());
            int ips = ipContainer.add(event.getAddress().getHostAddress());
            if (!users.containsKey(uuid)) users.put(uuid, ipContainer);

            if (Bukkit.getPluginManager().getPlugin("AdvancedBan") != null) {
                if (ips > 3 && !PunishmentManager.get().isBanned(UUIDManager.get().getUUID(event.getName()))) {
                    final String name = event.getName();
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban -s " + name + " Suspected Bot");
                            Bukkit.broadcast(ChatColor.DARK_RED + "[AutoBan] " + ChatColor.RED + "Banned " + name + " for trying to connect with too many IP's in the last hour.", "ontime.graylist.notify");
                        }
                    }.runTask(plugin);
                }
            }

            if (plugin.getOnTime(event.getUniqueId()) <= plugin.enabled) {
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_WHITELIST, plugin.getKickMessage());
            }
        }
    }
}
