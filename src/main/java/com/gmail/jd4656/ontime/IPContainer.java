package com.gmail.jd4656.ontime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IPContainer {
    private Map<String, Long> ips;

    IPContainer() {
        this.ips = new HashMap<>();
    }

    int add(String ip) {
        ips.put(ip, System.currentTimeMillis());
        List<String> ipsToRemove = new ArrayList<>();
        for (Map.Entry<String, Long> entry : ips.entrySet()) {
            String curIp = entry.getKey();
            long joinTime = entry.getValue();
            long age = (System.currentTimeMillis() - joinTime);

            long ageInMinutes = (age / 1000) / 60;

            if (ageInMinutes >= 60) {
                ipsToRemove.add(curIp);
            }
        }

        for (String curIp : ipsToRemove) {
            ips.remove(curIp);
        }

        return ips.size();
    }
}
