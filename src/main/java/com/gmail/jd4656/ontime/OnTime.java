package com.gmail.jd4656.ontime;

import com.earth2me.essentials.Essentials;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import static org.bukkit.ChatColor.COLOR_CHAR;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OnTime extends JavaPlugin {
    long enabled = -1;
    FileConfiguration config = null;

    private Connection connection = null;
    SQLConfiguration mysqlConfig;
    private static Economy econ = null;
    private static Essentials essentials = null;

    OnTime plugin;
    Map<UUID, Long> loginTime = new HashMap<>();
    Map<Player, Integer> paidTime = new HashMap<>();
    Set<String> nameCache = new HashSet<>();

    @Override
    public void onEnable() {
        plugin = this;
        this.saveDefaultConfig();
        config = this.getConfig();
        config.options().copyDefaults(false);
        this.saveConfig();

        setupEconomy();
        if (Bukkit.getPluginManager().getPlugin("Essentials") != null) {
            essentials = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");
        }

        mysqlConfig = new SQLConfiguration(config.options().configuration().getConfigurationSection("mysql"));

        plugin.getLogger().info("Creating database tables.");

        new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<Player, Integer> entry : paidTime.entrySet()) {
                    if (essentials != null && essentials.getUser(entry.getKey()).isAfk()) continue;
                    entry.setValue(entry.getValue() + 1);

                    if (entry.getValue() == 10) {
                        int payout = getOntimePayout(entry.getKey());
                        if (payout > 0 && econ != null) {
                            econ.depositPlayer(entry.getKey(), payout);
                            entry.getKey().sendMessage(ChatColor.GREEN + "You've received " + ChatColor.RED + "$" + payout + ChatColor.GREEN + " for being online 10 minutes.");
                        }

                        entry.setValue(0);
                    }
                }
            }
        }.runTaskTimer(plugin, 0, 60 * 20L);

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection conn = plugin.getConnection();
                    Statement stmt = conn.createStatement();
                    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + mysqlConfig.getTableName("players") + "(uuid VARCHAR(36) UNIQUE, name TEXT NOT NULL)");
                    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + mysqlConfig.getTableName("ontime") + "(uuid VARCHAR(36), loginTime BIGINT NOT NULL, duration BIGINT NOT NULL)");
                    stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + mysqlConfig.getTableName("version") + "(id VARCHAR(15) PRIMARY KEY DEFAULT '1', version VARCHAR(15) DEFAULT '1')");
                    stmt.executeUpdate("INSERT IGNORE INTO " + mysqlConfig.getTableName("version") + " (id, version) VALUES (1, 1)");

                    ResultSet rs = stmt.executeQuery("SELECT version FROM " + mysqlConfig.getTableName("version") + " WHERE id=1");
                    rs.next();
                    if (rs.getString("version").equals("1")) {
                        plugin.getLogger().info("Upgrading database from version 1 to version 2");
                        stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + mysqlConfig.getTableName("ontime") + "(uuid VARCHAR(36), loginTime BIGINT NOT NULL, duration BIGINT NOT NULL)");
                        rs = stmt.executeQuery("SELECT * FROM " + mysqlConfig.getTableName("players"));

                        PreparedStatement pstmt = null;

                        while (rs.next()) {
                            pstmt = conn.prepareStatement("INSERT INTO " + mysqlConfig.getTableName("ontime") + " (uuid, loginTime, duration) VALUES (?, ?, ?)");

                            pstmt.setString(1, rs.getString("uuid"));
                            pstmt.setLong(2, rs.getLong("joinTime"));
                            pstmt.setLong(3, rs.getLong("onTime"));

                            pstmt.executeUpdate();
                        }

                        rs.close();

                        stmt.executeUpdate("CREATE TABLE " +  mysqlConfig.getTableName("players2") + "(uuid VARCHAR(36) UNIQUE, name TEXT NOT NULL)");

                        rs = stmt.executeQuery("SELECT * FROM " + mysqlConfig.getTableName("players"));

                        while (rs.next()) {
                            pstmt = conn.prepareStatement("INSERT INTO " + mysqlConfig.getTableName("players2") + " (uuid, name) VALUES (?, ?)");
                            pstmt.setString(1, rs.getString("uuid"));
                            pstmt.setString(2, rs.getString("name"));
                            pstmt.executeUpdate();
                        }

                        if (pstmt != null) pstmt.close();

                        stmt.executeUpdate("DROP TABLE " + mysqlConfig.getTableName("players"));
                        stmt.executeUpdate("ALTER TABLE " + mysqlConfig.getTableName("players2") + " RENAME TO " + mysqlConfig.getTableName("players"));
                        stmt.executeUpdate("INSERT INTO " + mysqlConfig.getTableName("version") + " (id, version) VALUES (1, 2) ON DUPLICATE KEY UPDATE version=2");


                        plugin.getLogger().info("Database upgraded to version 2");
                    }

                    rs = stmt.executeQuery("SELECT name FROM " + mysqlConfig.getTableName("players"));

                    while (rs.next()) nameCache.add(rs.getString("name"));

                    stmt.close();


                    plugin.getLogger().info("Created database tables.");
                } catch (SQLException | ClassNotFoundException e) {
                    plugin.getLogger().warning("Error creating tables: ");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(this);

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("graylist").setExecutor(new CommandGraylist(this));
        this.getCommand("ontime").setExecutor(new CommandOnTime(this));

        getLogger().info("OnTime loaded.");
    }

    @Override
    public void onDisable() {
        try {
            Connection con = plugin.getConnection();
            for (Map.Entry<UUID, Long> entry : loginTime.entrySet()) {
                Player curPlayer = Bukkit.getPlayer(entry.getKey());
                if (curPlayer == null) continue;
                long joinTime = entry.getValue();
                plugin.updateTime(curPlayer, joinTime, (System.currentTimeMillis() - joinTime));
            }


        } catch (SQLException | ClassNotFoundException ignored) {}
    }

    public int getOntimePayout(Player player) {
        int payout = 0;
        for (int i=0; i<101; i++) {
            if (player.hasPermission("ontime.reward." + i)) payout = i;
        }

        return payout;
    }

    public String getKickMessage() {
        return ChatColor.translateAlternateColorCodes('&', String.join("\n", config.getStringList("kickMessage")));
    }

    public long getOnTime(UUID id) {
        return getOnTime(id, 0);
    }

    public long getOnTime(UUID id, long time) {
        long onTime = loginTime.getOrDefault(id, 0L);


        try {
            Connection con = plugin.getConnection();
            PreparedStatement pstmt = con.prepareStatement("SELECT SUM(duration) FROM " + mysqlConfig.getTableName("ontime") + " WHERE uuid=? AND loginTime >=?");
            pstmt.setString(1, id.toString());
            pstmt.setLong(2, time);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) onTime = rs.getLong("SUM(duration)");
            return onTime;
        } catch (SQLException | ClassNotFoundException e) {
            plugin.getLogger().warning("Error getting ontime for " + id.toString() + ": ");
            e.printStackTrace();

            return 0;
        }
    }

    public UUID getUUID(String name) {
        try {
            Connection con = plugin.getConnection();
            PreparedStatement pstmt = con.prepareStatement("SELECT uuid FROM " + mysqlConfig.getTableName("players") + " WHERE UPPER(name)=? LIMIT 1");
            pstmt.setString(1, name.toUpperCase());

            ResultSet rs = pstmt.executeQuery();
            if (!rs.next()) return null;
            return UUID.fromString(rs.getString("uuid"));
        } catch (SQLException | ClassNotFoundException e) {
            plugin.getLogger().warning("Error getting id for " + name + ": ");
            e.printStackTrace();

            return null;
        }
    }

    public long getJoinTime(UUID id) {
        long joinTime = 0;

        try {
            Connection con = plugin.getConnection();
            PreparedStatement pstmt = con.prepareStatement("SELECT loginTime FROM " + mysqlConfig.getTableName("ontime") + " WHERE uuid=? ORDER BY loginTime ASC LIMIT 1");
            pstmt.setString(1, id.toString());

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) joinTime = rs.getLong("loginTime");
            return joinTime;
        } catch (SQLException | ClassNotFoundException e) {
            plugin.getLogger().warning("Error getting ontime for " + id.toString() + ": ");
            e.printStackTrace();

            return 0;
        }
    }

    public long getLastOnline(UUID id) {
        long joinTime = 0;

        try {
            Connection con = plugin.getConnection();
            PreparedStatement pstmt = con.prepareStatement("SELECT loginTime FROM " + mysqlConfig.getTableName("ontime") + " WHERE uuid=? ORDER BY loginTime DESC LIMIT 1");
            pstmt.setString(1, id.toString());

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) joinTime = rs.getLong("loginTime");
            return joinTime;
        } catch (SQLException | ClassNotFoundException e) {
            plugin.getLogger().warning("Error getting ontime for " + id.toString() + ": ");
            e.printStackTrace();

            return 0;
        }
    }

    public void updateTime(Player player, long joinTime, long duration) {
        UUID uuid = player.getUniqueId();
        String playerName = player.getName();

        try {
            Connection con = plugin.getConnection();
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO " + mysqlConfig.getTableName("ontime") + "(uuid, loginTime, duration) VALUES (?, ?, ?)");
            pstmt.setString(1, uuid.toString());
            pstmt.setLong(2, joinTime);
            pstmt.setLong(3, duration);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement("INSERT INTO " + mysqlConfig.getTableName("players") + "(uuid, name) VALUES (?, ?) ON DUPLICATE KEY UPDATE name=?");
            pstmt.setString(1, uuid.toString());
            pstmt.setString(2, playerName);
            pstmt.setString(3, playerName);
            pstmt.executeUpdate();

            pstmt.close();
        } catch (SQLException | ClassNotFoundException e) {
            plugin.getLogger().warning("Error saving ontime for player " + playerName + ": ");
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return connection;
        }

        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection(mysqlConfig.getHostString(), mysqlConfig.getUser(), mysqlConfig.getPassword());
        return connection;
    }

    String convertTime(long ms) {
        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0 && (days < 1 && hours < 1 && minutes < 1)) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    String convertTimeColours(long ms, String daysHex, String hoursHex, String minutesHex) {
        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += parseColorCodes(daysHex) + days + (days == 1 ? " day " : " days ") + ChatColor.RESET;
        }
        if (hours > 0) {
            reply += parseColorCodes(hoursHex) + hours + (hours == 1 ? " hour " : " hours ") + ChatColor.RESET;
        }
        if (minutes > 0) {
            reply += parseColorCodes(minutesHex) + minutes + (minutes == 1 ? " minute " : " minutes ") + ChatColor.RESET;
        }
        if (seconds > 0 && (days < 1 && hours < 1 && minutes < 1)) {
            reply += parseColorCodes(minutesHex) + seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    long stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        long timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }


        if (timeModifier == 's') {
            timeDuration = timeNumber * 1000L;
        }
        if (timeModifier == 'm') {
            timeDuration = timeNumber * 60 * 1000L;
        }
        if (timeModifier == 'h') {
            timeDuration = timeNumber * 60 * 60 * 1000L;
        }
        if (timeModifier == 'd') {
            timeDuration = timeNumber * 24 * 60 * 60 * 1000L;
        }

        return timeDuration;
    }

    public String parseColorCodes(String str) {
        str = ChatColor.translateAlternateColorCodes('&', str);
        str = translateHexColorCodes("#", "", str);

        return str;
    }

    public String translateHexColorCodes(String startTag, String endTag, String message) {
        final Pattern hexPattern = Pattern.compile(startTag + "([A-Fa-f0-9]{6})" + endTag);
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find()) {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }
}
