package com.gmail.jd4656.ontime;

import org.bukkit.configuration.ConfigurationSection;

public class SQLConfiguration {

    private String host, port, databaseName, tablePrefix, user, password;

    SQLConfiguration(ConfigurationSection config) {
        this.host = config.getString("host");
        this.port = config.getString("port");
        this.databaseName = config.getString("database");
        this.tablePrefix = config.getString("table_prefix");
        this.user = config.getString("user");
        this.password = config.getString("password");
    }

    public String getTableName(String name) {
        return this.tablePrefix + name;
    }

    public String getHostString() {
        return "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.databaseName;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
